#pragma once

#include "ofMain.h"
#include "ofxGui.h"
#include "ofxArtnet.h"

class ofxArtnetDisplay {
    public:
        void setup();
        void update();
        void draw();
        ofxGuiGroup gui; 
}
