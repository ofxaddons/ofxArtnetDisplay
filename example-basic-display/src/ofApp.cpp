#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
    receiver.setup();
    texture.allocate(170, 1, GL_RGB); // Allocate a 170x1 RGB texture
}

//--------------------------------------------------------------
void ofApp::update(){
    if (receiver.hasMessage()){
        ofxArtnetMessage m;
        receiver.getData(m);
        data.resize(m.getSize());
        m.readTo(data.data());
        texture.loadData(data.data(), 170, 1, GL_RGB); // Load the Art-Net data into the texture
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    ofBackground(0);
    // Draw the texture stretched to the window size
    texture.draw(0, 0, ofGetWidth(), ofGetHeight());
    ofDrawBitmapStringHighlight(ofToString(ofGetFrameRate(), 2), 20, 20);
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
