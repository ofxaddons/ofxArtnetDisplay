#include "ofApp.h"

void ofApp::setup(){
    receiver.setup(PORT);
    videoTexture.allocate(NUM_PIXELS, 1, GL_RGB);
    memset(pixels, 0, NUM_PIXELS * 3 * sizeof(unsigned char));

    // Change the color order here if needed, e.g., to GRB
    //colorOrder = {1, 0, 2}; // G, R, B for GRB mapping
}

void ofApp::update(){
    while(receiver.hasWaitingMessages()){
        ofxOscMessage m;
        receiver.getNextMessage(m);

        if(m.getAddress() == "/dmx/1" && m.getArgType(0) == OFXOSC_TYPE_BLOB){
            ofBuffer blob = m.getArgAsBlob(0);
            int numPixels = blob.size() / 3;
            const unsigned char* blobData = reinterpret_cast<const unsigned char*>(blob.getData());

            for(int i = 0; i < numPixels && i < NUM_PIXELS; ++i){
                int blobIndex = i * 3;

                // Apply the color mapping
                for (int color = 0; color < 3; ++color) {
                    pixels[blobIndex + color] = blobData[blobIndex + colorOrder[color]];
                }
            }

            videoTexture.loadData(pixels, NUM_PIXELS, 1, GL_RGB);
        }
    }
}

void ofApp::draw(){
    videoTexture.draw(0, 0, ofGetWidth(), ofGetHeight());
}
