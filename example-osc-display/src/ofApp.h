#include "ofMain.h"
#include "ofxOsc.h"
#include <vector>

#define PORT 50004
#define NUM_PIXELS 170

class ofApp : public ofBaseApp{
    public:
        void setup();
        void update();
        void draw();

        ofxOscReceiver receiver;
        ofTexture videoTexture;
        unsigned char pixels[NUM_PIXELS * 3];

        // Define color order as a vector (RGB by default)
        std::vector<int> colorOrder {0, 1, 2}; // R=0, G=1, B=2
};
